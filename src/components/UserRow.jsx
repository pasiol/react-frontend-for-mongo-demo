import React from 'react';
import Delete from './Delete'

const UserRow = ({user,deleteUser}) => {

    const deleteRow = async (event) => {
        event.preventDefault();
        deleteUser({
            Username: user.Username,
            FirstName: user.FirstName,
            LastName: user.LastName,
        });
    }

    return (
        <>
            <td>{user.Username}</td><td>{user.FirstName}</td><td>{user.LastName}</td><td><Delete deleteRow={deleteRow} /></td>
        </>
    );
};

export default UserRow;
import React from 'react';
import UserRow from './UserRow'


const User = ({user, deleteUser}) => {
    return (
        <tr>
            <UserRow user={user} deleteUser={deleteUser}/>
        </tr>
    );
};

export default User;
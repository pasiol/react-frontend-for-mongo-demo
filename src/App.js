/* eslint-disable no-undef */
import React, { useState, useEffect } from 'react';
import userService from './services/users';
import User from './components/User'
import Table from 'react-bootstrap/Table'
import UserForm from './components/UserForm'


const App = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        userService.getAll().then((users) => {
            setUsers(users)
            console.log('data: ', users )
        });
        
    }, []);

    const upsertUser = async (userObject) => {
        try {
            console.log("userObject: ", userObject);
            await userService.upsert(userObject);
            const usersUpdate = await userService.getAsyncAll();
            setUsers(usersUpdate)
        } catch (error) {
            console.log("upserting failed: ", error);
        }
    }

    const deleteUser = async (deleteUser) => {
        console.log("deleteUser fired")
        try {
            console.log("delete user id: ", deleteUser.Username);
            await userService.remove(deleteUser);
            const usersUpdate = await userService.getAsyncAll();
            setUsers(usersUpdate);
        } catch (error) {
            console.log("deleting failed: ", deleteUser.Username, error)
        }
    }

    return (
        <>
            <h2>User</h2>
            <UserForm  upsertUser={upsertUser}/>
            <h2>Users</h2>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user) =>
                        <User
                            key={user.Username}
                            user={user} deleteUser={deleteUser}
                        />
                    )}
                </tbody>
            </Table>
        </>
    )
};

export default App;
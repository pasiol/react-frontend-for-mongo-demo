import React, {useState} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import FormControl from 'react-bootstrap/FormControl';
import FormText from 'react-bootstrap/FormText';


const UserForm = (props) => {
    console.log("UserForm props: ", props)
    const [username, setUsername] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [password, setPassword] = useState('');


    const upsertUser = async (event) => {
        event.preventDefault();
        console.log("data: ", username, firstName, lastName);
        if (username.length > 0 && firstName.length > 0 && lastName.length > 0) {
            props.upsertUser({
                Username: username,
                FirstName: firstName,
                LastName: lastName,
                Password: password,
            });
            setUsername('');
            setFirstName('');
            setLastName('');
            setPassword('');
        } else {
            console.log("Missing user information.");
        }
    };
    

    return (
        <form onSubmit={upsertUser}>
            Username: <input  type="text" value={username} name="username" onChange={({target}) => setUsername(target.value)}/> 
            First name: <input  type="text" value={firstName} name="firstname" onChange={({target}) => setFirstName(target.value)}/>
            Last name: <input  type="text" value={lastName} name="lastname" onChange={({target}) => setLastName(target.value)}/>
            Password: <input  type="password" value={password} name="password" onChange={({target}) => setPassword(target.value)}/>
            <button type="submit">Submit</button>
        </form>
    );
};

export default UserForm;
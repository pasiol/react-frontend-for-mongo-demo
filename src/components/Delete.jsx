import React from 'react';
import Button from 'react-bootstrap/Button';


const Delete = ({deleteRow}) => {
    return (
        <>
            <button type="submit" onClick={deleteRow}>Delete</button>
        </>
    );
};

export default Delete;
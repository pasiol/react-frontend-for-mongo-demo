import axios from 'axios';

const baseUrl = process.env.REACT_APP_API_URL
const origin = process.env.REACT_APP_ORIGIN

const getAll = () => {
  console.log("baseUrl: ", baseUrl)
  console.log("origin: ", origin)
  const request = axios.get(baseUrl +'/users');
  return request.then((response) => response.data);
};

const getAsyncAll = async () => {
  const response = await axios.get(baseUrl + '/users');
  return response.data;
}

const upsert = async (newObject) => {
  console.log("upsert newObject: ", newObject)
  const response = await axios.post(baseUrl + '/user', newObject);
  return response.data;
}

const remove = async (user) => {
  console.log("delete user id: ", user.Username)
  const response = await axios.delete(baseUrl + '/user/' + user.Username);
  return response.data;
}

export default { getAll, upsert, remove, getAsyncAll };